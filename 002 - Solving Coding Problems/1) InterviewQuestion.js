// Given 2 arrays, create a function that let's a user know (true/false) whether these two arrays contain any common items
//For Example:
//const array1 = ['a', 'b', 'c', 'x'];//const array2 = ['z', 'y', 'i'];
//should return false.
//-----------
//const array1 = ['a', 'b', 'c', 'x'];//const array2 = ['z', 'y', 'x'];
//should return true.

// 2 parameters - arrays - no size limit
// return true or false

const array1 = ['a', 'b', 'c', 'x'];
const array2 = ['z', 'y', 'a'];

function containsCommonItem(arr1, arr2) {
    for (let i=0; i < arr1.length; i++) {
      for ( let j=0; j < arr2.length; j++) {
        if(arr1[i] === arr2[j]) {
          return true;
        }
      }
    }
    return false
  }
  
containsCommonItem(array1, array2); //O(a*b) -> O(n^2) worst case if a === b
                                    //O(1) - Space Complexity as we do not allocate memory to finish the function


function containsCommonItem2(arr1, arr2){
    // loop through first array and create an object where if an element is contained, we mark it as true
    // Ex: [a,b,c] -> {{a: true}, {b: true}, {c: true}}
    let map = {};

    for (let i = 0; i < arr1.length; i++){
        if(!map[arr1[i]]){
            const item = arr1[i]; // a
            map[item] = true; // {a: true}
        }
    }

    // loop through 2nd arr and check if item in second arr exists in the map object
    for (let j = 0; j < arr2.length; j++){
        if(map[arr2[j]]){
            return true; 
        }
    }
    return false;
}

containsCommonItem2(array1, array2); // O(a+b) time compl;exity -> O(n) worst case
                                    // O(a) space complexity

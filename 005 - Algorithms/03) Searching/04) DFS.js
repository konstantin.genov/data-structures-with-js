/*
The DFS algorithm is a recursive algorithm that uses the idea of backtracking. 
It involves exhaustive searches of all the nodes by going ahead, if possible, else by backtracking.

Here, the word backtrack means that when you are moving forward and there are no more nodes along the current path,
you move backwards on the same path to find nodes to traverse. All the nodes will be visited on the current path till
all the unvisited nodes have been traversed after which the next path will be selected.

DFS is used to determine if a path exists.

Ref.: https://www3.cs.stonybrook.edu/~skiena/combinatorica/animations/search.html
https://visualgo.net/en/dfsbfs

Good use cases:
1. If the tree is very wide, a BFS might need too much memory, so it might be completely impractical.
2. If solutions are frequent but located deep in the tree, BFS could be impractical.

*/

/*
A linear search is the simplest method of searching a data set.

Starting at the beginning of the data set, each item of data is examined until a match is made.
Once the item is found, the search ends. If there is no match, the algorithm must deal with this.

A written description algorithm for a linear search might be:

1. Find out the length of the data set.
2. Set counter to 0.
3. Examine value held in the list at the counter position.
4. Check to see if the value at that position matches the value searched for.
5. If it matches, the value is found. Send a message and end the search.
6. If not, increment the counter by 1 and go back to step 3 until there are no more items to search.
7. If all the items have been checked and no match is found, send a message.

-------

Although simple, a linear search can be quite inefficient. Suppose the data set contained 100 items of data,
and the item searched for happens to be the last item in the set. 
All of the previous 99 items would have to be searched through first.

**However**, linear searches have the **advantage** that they will work on **any** data set, 
whether it is **ordered** or **unordered**.

*/

// E.g of built in linear searches in JS (much like Java)

let arr = ["Monkey", "Business", "Banana", "Rama"];

// linear O(n)
arr.indexOf("Business");

// linear O(n)
arr.findIndex(function (item) {
  return item === "Banana";
});

/*
BFS is a traversing algorithm where you should start traversing from a selected node (source or starting node) 
and traverse the graph layerwise thus exploring the neighbour nodes (nodes which are directly connected to source node).
You must then move towards the next-level neighbour nodes.

As the name BFS suggests, you are required to traverse the graph breadth-wise as follows:
1. First move horizontally and visit all the nodes of the current layer
2. Move to the next layer
3. Repeat process

This algorithm can get very expensive memory wise as you have to keep track of all of the nodes you've visited
and pointers to their children. 

Good use case scenarios for BFS are:
1. The value you are looking for resides close to the top of tree/graph
2. If the tree is very deep and solutions are rare, depth first search (DFS) might take an extremely 
long time, but BFS could be faster.

Questionable use cases for BFS:
1. If the tree is very wide, a BFS might need too much memory, so it might be completely impractical.
2. If solutions are frequent but located deep in the tree, BFS could be impractical.

Ref.: https://visualgo.net/en/dfsbfs 
*/

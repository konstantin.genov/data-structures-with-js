/*
Recursion is when you define something in terms of itself. Simply, it is a function that refers to itself
inside of the function. The problem with recursion and why people find it so difficult is that it's 
an intimidating topic when people start talking about math and theoretical applications.

Instead, let's focus on how recursion helps you solve problems. Recursion is really good for tasks that have
repeated subtasks to do. It is widely used in searching and sorting algorithms - for example: traversing a tree.

E.g below: */
// simple function with recursion which will lead to stack overflow
function recursion() {
  recursion();
}

/*
Every recursive function needs to have something called a base case or a stop point. Recursive functions have
2 paths. One is the recursive case -> call the function again and run it, and then the base case -> stop calling the function.
We need this to stop the function from running us out of memory, causing a stack overflow.
E.g: */
let counter = 0;
function recursionProper() {
  console.log(counter);
  if (counter > 10) {
    console.log("Done!");
    return "Done?"; // this will return undefined, as the return variable is popped of the stack and
  } // will no longer be in memory. In order to return it, we need to bubble up the value to the top.
  counter++;
  recursionProper(); // will not return 'Done?'
  // return recursionProper() -> will return 'Done?' as the function knows to return the previous value of the function call
  // this way, we are passing the return value from the top of the stack to the bottom
}

recursionProper();

// Remember these 3 rules:
// 1. Define the base case
// 2. Identify the recursive case
// 3. Get closer and closer and return when needed. Usually you have 2 returns:
// for the base case and for the recursive case (as illustrated above)

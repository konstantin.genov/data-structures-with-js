/*
Merge sort is one of the most efficient sorting algorithms. O(n log n)
It works on the principle of **Divide and Conquer**. 
Merge sort repeatedly breaks down a list into several sub-lists until each sub-list consists of a single element
and merging those sub-lists in a manner that results into a sorted list.

Idea:
- Divide the unsorted list into N sub-lists, each containing 1 element.
- Take adjacent pairs of two singleton lists and merge them to form a list of 2 elements.
N will now convert into N/2 lists of size 2.
- Repeat the process till a single sorted list of obtained.
While comparing two sub-lists for merging, the first element of both lists is taken into consideration.
While sorting in ascending order, the element that is of a lesser value becomes a new element of the sorted list.
This procedure is repeated until both the smaller sub-lists are empty and the new combined sub-list comprises all 
the elements of both the sub-lists.

Ref.: https://visualgo.net/bn/sorting?slide=1 for visualization
*/
const numbers = [99, 44, 6, 2, 1, 5, 63, 87, 283, 4, 0];

function mergeSort(array) {
  if (array.length === 1) {
    // base case - end the recursion
    return array;
  }
  // Split Array in into right and left
  const length = array.length;
  const middle = Math.floor(length / 2);
  const left = array.slice(0, middle);
  const right = array.slice(middle);
  // console.log('left:', left); // uncomment to watch the process
  // console.log('right:', right);

  return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
  const result = []; //fill sorted elements here
  let leftIndex = 0;
  let rightIndex = 0;
  //loop while we have items in our arrays
  while (leftIndex < left.length && rightIndex < right.length) {
    // compare items and push accordingly
    if (left[leftIndex] < right[rightIndex]) {
      result.push(left[leftIndex]);
      leftIndex++;
    } else {
      result.push(right[rightIndex]);
      rightIndex++;
    }
  }
  // console.log(left, right)
  return result.concat(left.slice(leftIndex)).concat(right.slice(rightIndex));
}

const answer = mergeSort(numbers);
console.log(answer);

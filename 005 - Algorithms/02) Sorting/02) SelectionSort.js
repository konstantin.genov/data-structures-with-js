/*
Selection sort divides the input list into two parts: a sorted sublist of items which is built up from left
to right at the front (left) of the list and a sublist of the remaining unsorted items that occupy 
the rest of the list. Initially, the sorted sublist is empty and the unsorted sublist is the entire input list.
The algorithm proceeds by finding the smallest (or largest, depending on sorting order) element in 
the unsorted sublist, exchanging (swapping) it with the leftmost unsorted element (putting it in sorted order),
and moving the sublist boundaries one element to the right.
The time efficiency of selection sort is quadratic - O(n^2)
Ref: https://www.youtube.com/watch?v=Ns4TPTC8whw&ab_channel=AlgoRythmics :D
*/
// Input -> 5, 17, 13, 27, 33, 69, 3, 9, 1, 7
function selectionSort(arr) {
  let length = arr.length;
  for (let i = 0; i < length; i++) {
    let temp = arr[i];
    let indexOfSmallestItem = i;
    for (let j = i + 1; j < length; j++) {
      if (arr[indexOfSmallestItem] > arr[j]) {
        indexOfSmallestItem = j;
      }
    }
    arr[i] = arr[indexOfSmallestItem];
    arr[indexOfSmallestItem] = temp;
  }
}
let numbers = [5, 17, 13, 27, 33, 69, 3, 9, 1, 7];
selectionSort(numbers);
console.log(numbers);

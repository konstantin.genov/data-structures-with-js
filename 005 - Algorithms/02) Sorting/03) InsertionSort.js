/*
Up until this point, I've mentioned 2 algorithms which aren't very efficient and aren't that commonly used.
Now, insertion sort is not the most efficient algorithm either, but there's cases where it's extremely fast:
The sort is pretty useful when you are pretty sure the list is almost sorted or sorted.
This might sound a bit silly,but sometimes this is important in computer science, 
and in a best case scenario you can get O(n)/linear time complexity.

Ref.: http://cs.armstrong.edu/liang/animation/web/InsertionSort.html for visualization
https://media.geeksforgeeks.org/wp-content/uploads/insertionsort.png
How it works: Insertion sort iterates, consuming one input element each repetition, and growing a sorted output list.
At each iteration, insertion sort removes one element from the input data, finds the location it belongs 
within the sorted list, and inserts it there. It repeats until no input elements remain.
*/

function insertionSort(arr) {
  for (let j = 1; i < arr.length; j++) {
    let startAdjacent = arr[j]; // keep reference to second element (next to starting) - first iteration -> arr[1]
    let startingElement = j - 1; // variable to help us remember/locate the starting element - arr[0];

    while (startingElement > -1 && arr[startingElement] > startAdjacent) {
      // First iteration example:
      // If the element at index 0 is greater than the element next to it (index 1)
      // Sample input: 5[0], 3[1] , 4[2], 9[3]
      // Perform first half of the swap, making arr[1] equal to arr[0] -> 3 is now 5
      arr[startingElement + 1] = arr[startingElement];
      startingElement--;
      // NOTE: Now we have 5 duplicated -> 5[0], 5[1], etc. We need to complete the swap
    }
    // Complete the swap, so arr[0] is now 3
    arr[startingElement + 1] = startAdjacent; // we started with 3[1] adjacent to 5[0]
  }
}

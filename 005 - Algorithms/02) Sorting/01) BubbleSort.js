/*
Bubble sort is a simple sorting algorithm that repeatedly steps through the list, 
compares adjacent elements and swaps them if they are in the wrong order. 
The pass through the list is repeated until the list is sorted. 
The algorithm, which is a comparison sort, is named for the way smaller or larger elements "bubble"
to the top of the list.

This simple algorithm performs POORLY in real world use and is used primarily as an **educational** tool
Time complexity: O(n^2).
*/

function bubbleSort(arr) {
  for (let i = 0; i < arr.length - 1; i++) {
    for (let j = i; j < arr.length; j++) {
      if (arr[i] > arr[j]) {
        let temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
      }
    }
  }
  return arr;
}

console.log(bubbleSort(new Array(5, 17, 13, 27, 33, 69, 3, 9, 1, 7)));

class Node {
    constructor(value){
        this.value = value;
        this.next = null;
    }
}

class Stack{
    constructor(){
        this.top = null;
        this.bottom = null;
        this.length = 0;
    }
    //get top/end element
    peek() { // O(1)
        return this.top.value;
    }
    //add new element
    push(value){ // O(1)
        const newNode = new Node(value);
        if(this.length === 0) { // if empty, first node is top & bottom of the stack
            this.top = newNode;
            this.bottom = newNode;
        } else { // if nodes present, new node is always on top
            const holdingPointer = this.top;
            this.top = newNode;
            this.top.next = holdingPointer;
        }
        this.length++;
        return this;
    }
    //remove top/end element
    pop(){ // O(1)
        if(!this.top){
            return null;
        }
        if(this.top === this.bottom) {
            this.bottom = null;
        }
        const deletedNode = this.top;
        this.top = this.top.next;
        this.length--;
        return deletedNode;
    }
    isEmpty(){
        if(this.length === 0){
            return true;
        } else {
            return false;
        }
    }
}

let myStack = new Stack();
myStack.push(1);
myStack.push(2);
myStack.push(3);
// myStack.peek();
myStack.pop();
console.log(myStack);
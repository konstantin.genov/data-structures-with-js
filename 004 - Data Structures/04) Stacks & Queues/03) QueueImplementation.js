class Node{
    constructor(value){
        this.value = value;
        this.next = null;
    }
}

class Queue{
    constructor(){
        this.first = null;
        this.last = null;
        this.length = 0;
    }
    //get element at beginning
    peek() {
        return this.first;
    }
    //add element to queue
    enqueue(value){
        const newNode = new Node(value);
        if(this.length === 0){
            this.first = newNode;
            this.last = newNode;
        } else {
            this.last.next = newNode; // when adding the second element, the 1st element first & last are equal, so in essence we are saying this.first.next
            this.last = newNode;
        } 
        this.length++;
    }
    //pop the first element in the queue
    dequeue(){
       if(!this.first){
           return null;
       }
       if (this.first === this.last){
           return this.last = null;
       }
       this.first = this.first.next; // remove 1st element and rotate next one into it's position
       this.length--;
    }
}

const myQueue = new Queue();
myQueue.enqueue(1);
myQueue.enqueue(2);
myQueue.enqueue(3);
myQueue.dequeue();
console.log(myQueue);

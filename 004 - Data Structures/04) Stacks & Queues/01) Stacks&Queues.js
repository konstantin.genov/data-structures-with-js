/*
Stacks and queues are very similar. They are both linear data structures and linear data structures allow us to traverse through 
data elements sequentially (one by one), in which only one data element can be directly reached.

The reason that these are very similar is that they can be implemented in similar ways, and the main difference is only how items get removed.
Unlike an array - in stacks & queues there's no random access operation. You mainly run functions like push, peak & pop which deal with the first
or the last element in the structure.

Q: You might ask, why would we use these data structures over an array?
A: Unlike arrays and linked lists we have less methods/actions that we can perform on S&Q. You sometimes might want to 
limit the operations you can do on data structures. You want the to track which element of the stack has been removed basing on what is left.
Imagine you hire a mechanic to fix your car and you give him 5 suitcases filled with tools, he will need some time to get familiar 
with all them and decide which ones he needs to finish the job. It would be better to just hand him the tools he needs for the job
and not over complicate things. : )


*** The Stack is a LIFO (last in, first out) list. It is a method for handling data structures where the first element is processed first and 
the newest element is processed last. (Give example of execution stack in programming languages; example in JS why your browser might freeze;
back button in your browser, CTRL+Z (undo). etc.)


E.g : 
Let’s say a stack of magazines, and you ordered it alphabetically, you will know that A and B titled magazines are removed by just looking on 
what’s left because you happen to see C. This way stacks actually will give you the idea right away where to begin processing. 
Unlike arrays, you’ll have to re-order it over and over again and look at every possible reason on why the array end up this way.

*** The Queue is a FIFO (first in, first out) list. It is a method for handling data structures where the last element is processed first and
the first element is processed last.

*/

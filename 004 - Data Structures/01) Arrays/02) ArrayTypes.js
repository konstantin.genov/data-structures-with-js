/*
There are two types of array:
- Static Array
- Dynamic Array

Static Arrays are arrays that are declared before runtime and are assigned values while writing the code, whilst Dynamic Arrays are arrays that are declared 
while writing the code and are assigned values in run-time.
E.g:
int arr[] = { 1, 3, 4 }; // static integer array.
ArrayList<Integer> arr = new ArrayList<>();; // dynamic integer array.

The one limitation of static arrays is that they are FIXED in size, meaning you will need to specify the number of elements your array will hold ahead of time.
There's no guarantee that after you've allocated the 7 'shelves' in memory that you can keep adding things on the array, especially in order. 
We solve this problem by introducing dynamic arrays, they allow us to copy and rebuild an array in a new location with more memory.

E.g:
We declare a static array String[] shoppingList = new String[7]; and we realize we have forgotten to add one more item to our list (increasing it to 8).
To fix this, we create a dynamic array, copy the entire shoppingList array and paste the 8th item we forgot.  
*/


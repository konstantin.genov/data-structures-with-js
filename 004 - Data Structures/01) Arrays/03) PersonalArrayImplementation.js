// Personal implementation of an array

class MyArrayImplementation {
    constructor() {
        this.length = 0;
        this.data = {};
    }

    get(index){
        return this.data[index];
    }

    //E.g:
    //When the array is first instantiated, length = 0, so when we do our first push the element will be at the end. 
    //Then the length variable is incremented, thus every new addition will always be at the end, as the previous would be at index length-1
    push(element) { // O(1)
        this.data[this.length] = element; //set item as last value in the object
        this.length++;
        return this.length;
    }

    // The last element is always at (length-1) as we use the length var as a reference point where a new addition should be added
    pop(element) { // O(1)
        const lastElement = this.data[this.length-1];
        delete lastElement;
        this.length--;
        return lastElement;
    }

    deleteAtIndex(index) { //O(n)
    const element = this.data[index];
    this.shiftItems
    }

    // function to reposition elements after an element at a specific index is deleted
    shiftItems(index){
        for(let i = index; i < this.length - 1; i++){
            this.data[i] = this.data[i+1];
        }
        delete this.data[this.length-1]; //as the loop stops at (length-1), make sure last item is removedd
        this.length--;
    }

}
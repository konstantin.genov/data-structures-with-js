/*
An array is a series of variables, depending on the language each variable (called array element) has the same data type.

When you define the size of the array, all of that space in memory is “reserved” from that moment on for future values that you may want to insert.
If you do not fill the array with values, that space will be kept reserved and empty until you do.

Elements of the array are stored sequentially in memory, thus access operations performed an an array are constant time.
For example, if an integer array has five elements and base address is 1000 and suppose the 
integer is taking two bytes of space in the memory then:

address of A[0]=1000 // base address of the array

address of A[1]=1002

address of A[2]=1004

address of A[3]=1006

address of A[4]=1008
*/

const strings = ['a', 'b', 'c', 'd'];
// In a 32-bit OS the following array will take 4*4 = 16 bytes of storage

//push (add)
strings.push('e'); // O(1) as we always at to the end of the array no calculation needs to be performed

//pop (remove)
strings.pop(); // O(1) same reason as above

//unshift (adds element in the beginning of the array)
strings.unshift('x'); // O(n) - the collection has to move each element to the right, repeated N times(size of arr)

//splice (add element at index)
strings.splice(2, 0, 'spookySkeleton') // O(n/2) -> O(n)

console.log(strings);
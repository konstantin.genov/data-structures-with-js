class Graph {
  constructor() {
    this.numberOfNodes = 0;
    this.adjacentList = {};
  }
  addVertex(node) {
    this.adjacentList[node] = []; // add arr to represent edges
    this.numberOfNodes++;
  }

  addVertex2(node) {
    this.adjacentList[node] = new Array(this.numberOfNodes).fill(0);
    this.numberOfNodes++;
  }

  //uniderected Graph
  addEdge(node1, node2) {
    this.adjacentList[node1].push(node2);
    this.adjacentList[node2].push(node1);
  }

  addEdge2(node1, node2) {
    this._balanceArrays();
    this.adjacentList[node1][node2] = 1;
    this.adjacentList[node2][node1] = 1;
  }
  _balanceArrays() {
    for (let i = 0; i < 10; i++) {
      if (this.adjacentList[i] !== undefined) {
        let arraySize = this.adjacentList[i].length;
        if (arraySize !== this.numberOfNodes) {
          while (arraySize !== this.numberOfNodes) {
            this.adjacentList[i].push("0");
            arraySize++;
          }
        }
      }
    }
  }
  showConnections() {
    const allNodes = Object.keys(this.adjacentList);
    for (let node of allNodes) {
      let nodeConnections = this.adjacentList[node];
      let connections = "";
      let vertex;
      for (vertex of nodeConnections) {
        connections += vertex + "";
      }
      console.log(node + "--> " + connections);
    }
  }
}

var myGraph = new Graph();
// myGraph.addVertex("0");
// myGraph.addVertex("1");
// myGraph.addVertex("2");
// myGraph.addVertex("3");
// myGraph.addVertex("4");
// myGraph.addVertex("5");
// myGraph.addVertex("6");
// myGraph.addEdge("3", "1");
// myGraph.addEdge("3", "4");
// myGraph.addEdge("4", "2");
// myGraph.addEdge("4", "5");
// myGraph.addEdge("1", "2");
// myGraph.addEdge("1", "0");
// myGraph.addEdge("0", "2");
// myGraph.addEdge("6", "5");
myGraph.addVertex2("0");
myGraph.addVertex2("1");
myGraph.addVertex2("2");
myGraph.addVertex2("3");
myGraph.addEdge2("1", "3");
myGraph.showConnections();

/*
Graphs are one of the most useful and most used data structures in computer science when it comes to real life.
In short, graphs are simply a set of values that are related in a pair-wise fashion - like a little network.

In a graph, each item is called a Node (or a Vertex). Nodes are then connected with edges (lines) with 
each other. As you can imagine, graphs are a great DS to model real world relationships, representing links.
We can use graphs to represent friendships, family trees, network, the internet, 
roads (one city to another, as they are connected), etc.

Facebook uses graphs for their social network;
Amazon uses them for their recommendation engines;
Google maps uses graphs to determine the shortest path to where you want to go;

-------------

Directed vs. Undirected graphs

Directed graphs are like one way streets. You can go into the street, but you can't go back. 
That will be the relationship between the vertexes in the graph with a directed one. These types of graphs
are useful for describing traffic flow, some kind of a system where the movement isn-t bi-directional.
E.g:
Twitter uses directed graphs for your followers. Someone might be following you, but you do not automatically
follow them, thus the relationship is one way. 
===========
A good way to think about an undirected graph is to think of the highway between 2 cities. You can go 
back and forth between the two of them.

Another good e.g:
Q: What kind of graph do you think Facebook uses for your friends?
A: An undirected graph, if you are connected to a friend, that friend is also connected to you - it's not one way.

*/

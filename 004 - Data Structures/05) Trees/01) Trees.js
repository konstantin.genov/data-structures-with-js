/*
Trees are nonlinear data structures composed of nodes that hold data. These nodes are connected by edges that define a parent and child relationship.
Trees are nonlinear in that they are not organized sequentially, instead they are organized through relationships or hierarchy. 
The first node of a tree is called the root node and it can have zero or more child nodes. A child node in a tree forms a subtree, 
where that child can have zero or more child nodes of its own, and so on (as you can see there’s a recursive approach to trees)
To better visualize what a tree is you can think of a tree as a hierarchical org chart of a company, an animal classification tree in biology,
or the Document Object Model (DOM).

A binary search tree (BST) is a binary tree where each node has a Comparable key (and an associated value) and satisfies the restriction that 
the key in any node is larger than the keys in all nodes in that node's left subtree and smaller than the keys in all nodes in that node's right subtree.
Ordering property: for all nodes N: all left descendants <= n < all right descendants. What defines a node as less than or greater than another node depends on your data type.

** Talk about balanced vs unbalanced trees, explain how we balance trees.
*/
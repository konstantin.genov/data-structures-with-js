// A node in a BST can have n >= 0 && n <= 2 sub-nodes (children). We always start with a root node, which will be followed by child nodes.
class Node {
  constructor(value) {
    this.value = value;
    this.right = null; //right child (bigger value)
    this.left = null; //left child (smaller value)
  }
}

class BinarySearchTreeImpl {
  constructor() {
    this.root = null; //start with no root element
  }
  insert(value) {
    const newNode = new Node(value);
    if (this.root === null) {
      this.root = newNode;
    } else {
      let currentNode = this.root; //start traversal from root
      while (true) {
        if (value < currentNode.value) {
          //value less than root, go left
          if (!currentNode.left) {
            // no node on left
            currentNode.left = newNode;
            return this;
          }
          currentNode = currentNode.left; //continue traversal
        } else {
          //value bigger than root, go right
          if (!currentNode.right) {
            // node node on right
            currentNode.right = newNode;
            return this;
          }
          currentNode = currentNode.right;
        }
      }
    }
  }
  lookup(value) {
    let currentNode = this.root;
    if (this.root === null) {
      return false;
    }
    while (true) {
      if (value < currentNode.value) {
        //go left
        currentNode = currentNode.left;
      } else if (value > currentNode.value) {
        //go right
        currentNode = currentNode.right;
      } else if (value === currentNode.value) {
        // match found
        return currentNode;
      } else {
        //no match
        return false;
      }
    }
  }
  remove(value) {
    if (this.root === null) {
      throw "The tree is empty!";
    }
    let currentNode = this.root;
    let parentNode = null; //keep reference to the parent node

    //traverse to find the node in question
    while (currentNode) {
      if (value < currentNode.value) {
        parentNode = currentNode;
        currentNode = currentNode.left;
      } else if (value > currentNode.value) {
        parentNode = currentNode;
        currentNode = currentNode.right;
      } else if (value === currentNode.value) {
        // Match found. Now let's get to work. We have 3 scenarios.

        //Scenario 1: node to be deleted has no right child
        if (currentNode.right === null) {
          if (parentNode === null) {
            // if we're deleting the root element, there will be no parent
            this.root = currentNode.left;
          } else {
            if (currentNode.value < parentNode.value) {
              // this means the current node we are deleting was located on the left of the parent
              parentNode.left = currentNode.left;
            } else if (currentNode.value > parentNode.value) {
              // the node was located on the right
              parentNode.right = currentNode.left;
            }
          }
          // Scenario 2: node to be deleted has a right child, but the child doesn't have a left leaf
        } else if (currentNode.right.left === null) {
          if (parentNode === null) {
            this.root = currentNode.left;
          } else {
            if (currentNode.value < parentNode.value) {
              // node was on the left of parent
              parentNode.left = currentNode.right; //always take the highest value, now right is parent to left
            } else if (currentNode.value > parentNode.value) {
              // node was on right of parent
              parentNode.right = currentNode.right;
            }
          }
        } else {
          //find the right child's left most child
          let leftmost = currentNode.right.left;
          let leftmostParent = currentNode.right;
          while (leftmost.left !== null) {
            leftmostParent = leftmost;
            leftmost = leftmost.left;
          }

          //parent's left subtree is now leftmost's right subtree
          leftmostParent.left = leftmost.right;
          leftmost.left = currentNode.left;
          leftmost.right = currentNode.right;

          if (parentNode === null) {
            this.root = leftmost;
          } else {
            if (currentNode.value < parentNode.value) {
              parentNode.left = leftmost;
            } else if (currentNode.value > parentNode.value) {
              parentNode.right = leftmost;
            }
          }
        }
      }
    }
  }
}

const myBST = new BinarySearchTreeImpl();
myBST.insert(16);
myBST.insert(10);
myBST.insert(20);
myBST.remove(10);
console.log(JSON.stringify(traverse(myBST.root)));

function traverse(node) {
  const tree = { value: node.value };
  tree.left = node.left === null ? null : traverse(node.left);
  tree.right = node.right === null ? null : traverse(node.right);
  return tree;
}

/*
A hash table is a data structure that is used to store keys/value pairs. 
It uses a hash function to compute an index into an array in which an element will be inserted or searched.
By using a good hash function, hashing can work well. Under reasonable assumptions, 
the average time required to search for an element in a hash table is O(1). A hash function is any function that can be used to map
a data set of an arbitrary size to a data set of a fixed size, which falls into the hash table. 
The values returned by a hash function are called hash values, hash codes, hash sums, or simply hashes.

Search: O(1)
Insert: O(1)
Algorithm: Average
Space: O(n)
Delete: O(1)
*/


// Storing key/value pairs has it's downsides. The hashing is always performed on the key, and with all hashes being one-way (from plain to hash)
// this poses some limitations - we cannot use the same key for multiple entries as hashing a value will always resort in the same hash

// E.g with MD5 hash:
// Imagine we create some sort of registry which uses first + last names of a person as the key, we add an entry for Jane Doe:
// Jane Doe -> hash ('Jane Doe') -> 1c272047233576d77a9b9a1acfdf741c (resulting MD5 hash). 
// Encoding the same string using the MD5 algorithm will ALWAYS result in the same 128-bit hash output!! 
// Now we've ran into a slight problem. When another Jane Doe would have to be registered, 
// we will encounter a *hash collision* as the hash value for the key is already present in our collection. 
// We can solve this by using separate chaining, linear probing, quadratic probing, double hashing, etc. but degrades performance O(1) -> O(n)
// This is a good example why unique identifiers like social security numbers (EGN) exist in the real world.

// Collisions visualized - https://www.cs.usfca.edu/~galles/visualization/OpenHash.html

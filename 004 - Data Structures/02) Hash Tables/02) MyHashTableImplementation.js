class HashTableImp {
    constructor(size){
        this.data = new Array(size);
    }

    _hash(key){
        let hash = 0;
        for (let i = 0; i < key.length; i ++){
            hash = (hash + key.charCodeAt(i) * i) % this.data.length;
        }
        return hash;
    }

    set(key, value) {
        let address = this._hash(key);
        if(!this.data[address]){ // check for collision
            this.data[address] = []; // new arr for key/value pair
            this.data[address].push(key, value);
        } else {
            this.data[address].push(key, value); // in case of collision, push to same arr
        }
    }

    get(key) {
        let address = this._hash(key);
        const currentBucket = this.data[address];
        if (!currentBucket) { // nothing in memory
            return undefined;
        }
        
        // Check if there's only one key/value pair to avoid looping
        if (currentBucket.length === 2) { // O(1) time complexity
            console.log(currentBucket[1]);
            return;
        }
        
        for (let i = 0; i < currentBucket.length; i++){ // Loop through all records at address
            if(currentBucket[i] === key) {
                console.log(currentBucket[i + 1]); // This is where the performance starts to degrade as we need to loop. O(n) time complexity
            }
        }
    }

    // returns all keys in the hash table

    keys() {
        const keysArr = [];
        for (let i = 0; i < this.data.length; i++){
            if(this.data[i]){
                keysArr.push(this.data[i][0]); // fetch and push 1st element from HT as arrays are nested
            }
        }
        return keysArr;
    }
}

const myHashTableImpl = new HashTableImp(4);
myHashTableImpl.set("Stonks", 1000);
myHashTableImpl.set("Stonks", 500);
myHashTableImpl.set("Stonks", 500);
myHashTableImpl.set("Stonks", 500);
myHashTableImpl.set(",mkkkkkkkTest", 50);
// myHashTableImpl.get("k45"); -- address not found
// myHashTableImpl.get(",mkkkkkkkTest");
console.log(JSON.stringify(myHashTableImpl.keys()));
class DoublyLinkedListImpl {
    constructor(value) {
        this.head = {
            value: value,
            next: null,
            previous: null
        }
        this.tail = this.head;
        this.length = 1;
    }
    append(value){ // O(1)
        const newNode = new Node(value);
        newNode.previous = this.tail;
        this.tail.next = newNode;
        this.tail = newNode;
        this.length++;
    }
    prepend(value){ // O(1)
        const newNode = new Node(value);
        newNode.next = this.head;
        this.head.previous = newNode;
        this.head = newNode;
        this.length++;
    }
    //insert node at index
    insert(index, value){
        this._indexValidation(index);
        const newNode = new Node(value);
        const nodeAtIndex = this._traverseToIndex(index-1);
        const nodeAfterIndex = nodeAtIndex.next;
        nodeAtIndex.next = newNode;
        newNode.next = nodeAfterIndex; 
        newNode.previous = nodeAtIndex;
        nodeAfterIndex.previous = newNode;
        this.length++;
    }
    remove(index) {
        this._indexValidation(index);
        const nodeAtIndex = this._traverseToIndex(index-1);
        const unwantedNode = nodeAtIndex.next;
        nodeAtIndex.next = unwantedNode.next;
        nodeAtIndex.previous = unwantedNode.previous;
        this.length--;
        return this.printList();
    }
    printList(){ // O(n)
        let currentNode = this.head;
        let nodesList = [];
        while(currentNode !== null){
            console.log(currentNode)
            nodesList.push(currentNode.value);
            currentNode = currentNode.next;
        }
        console.log(nodesList);
        return nodesList;
    }
    _traverseToIndex(index){ 
        let counter = 0;
        let currentNode = this.head;
        while (counter !== index){
            currentNode = currentNode.next;
            counter++;
        }
        return currentNode;
    }

    _indexValidation(index) {
        if (index > this.length || index < 0) {
            throw 'Index out of bounds exception!';    
        }
    }
}

class Node{
    constructor(value){
        this.value = value;
        this.next = null;
        this.previous = null;
    }
}


let myLinkedList = new DoublyLinkedListImpl(10);
myLinkedList.append(5);
myLinkedList.append(6);
// myLinkedList.prepend(1);
// myLinkedList.insert(1, 99);
myLinkedList.remove(1);
myLinkedList.printList();
// console.log(myLinkedList);
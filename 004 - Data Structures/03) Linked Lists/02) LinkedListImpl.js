// 69 --> 53 --> 19

// Representation of what I want to achieve
let linkedListImplementationExample = {
    head: {
        value: 69,
        next: {
            value: 53,
            next: {
                value: 19,
                next: null
            }
        }
    }
}

class LinkedListImpl {
    constructor(value) { // need to instantiate the head node
        this.head = {
            value: value,
            next: null
        }
        this.tail = this.head; // at creation the tail is the head, as we have only 1 node
        this.length = 1;
    }
    append(value){ // O(1)
        const newNode = {
            value: value,
            next: null
        }
        this.tail.next = newNode; //update next property of previous node
        this.tail = newNode; //update end to current node
        this.length++;
    }
    //insert at beginning of the linked list
    prepend(value){ // O(1)
        const newNode = {
            value: value,
            next: this.head // next is previous head
        }
        this.head = newNode; //head of LL is now the new node
        this.length++;
    }
    //insert node at index
    insert(index, value){
        this._indexValidation(index);
        // 1, 2, 3, 4 -> insert number 5 at index 1 -> 2 --> 5; 5 --> 3 
        const newNode = {
            value: value,
            next: null
        }
        const nodeAtIndex = this._traverseToIndex(index-1);
        const holdingPointer = nodeAtIndex.next; // keep reference of node at the index's next pointer
        nodeAtIndex.next = newNode;
        newNode.next = holdingPointer;
        this.length++;
    }
    //remove node at index
    remove(index) {
        this._indexValidation(index);
        // 10[0], 16[1], 17[2], 14[3], 99[4] -> remove node at index 2 (17) -> 10, 16, 14, 99  
        const nodeAtIndex = this._traverseToIndex(index-1);
        const unwantedNode = nodeAtIndex.next; // 16.next ---> 17
        nodeAtIndex.next = unwantedNode.next; // 16.next = 17.next (99); 16 doesn't point to 17, so GC takes care of the rest 
        this.length--;
        return this.printList();
    }
    printList(){ // O(n)
        let currentNode = this.head;
        let nodesList = [];
        while(currentNode !== null){
            nodesList.push(currentNode.value);
            currentNode = currentNode.next;
        }
        console.log(nodesList);
        return nodesList;
    }
    _traverseToIndex(index){ // O(n) worst case, however, it will rarely be 0(n)
        let counter = 0;
        let currentNode = this.head; // for traversal
        while (counter !== index){
            currentNode = currentNode.next;
            counter++;
        }
        return currentNode;
    }

    _indexValidation(index) {
        if (index > this.length || index < 0) {
            throw 'Index out of bounds exception!';    
        }
    }
}


let myLinkedList = new LinkedListImpl("10");
myLinkedList.append(5);
myLinkedList.append(16);
myLinkedList.prepend(1);
myLinkedList.insert(2, 99);
// myLinkedList.insert(20, 88);
myLinkedList.remove(2);

// Linked Lists

/*
One disadvantage of using arrays to store data is that arrays are static structures and therefore cannot be easily extended or reduced to fit the data set.
Arrays are also expensive to maintain new insertions and deletions. Linked lists are a data structure that addresses some of the limitations of arrays.

A linked list is a linear data structure where each element is a separate object.
Each element (we'll call it a node) of a list is compromised of two items - the data and a reference to the next node.
The last node has a reference to **null**. The entry point into a linked list is called the **head** of the list.
It should be noted that head is not a separate node, but the reference to the first node. If the list is empty then the head is a null reference.  

A linked list is a dynamic data structure. The number of nodes in a list is not fixed and can grow and shrink on demand.
In an array, the size is doubled every time we fill up the capacity and when. Also, when we delete elements from the array we do NOT free memory. 

Any application which has to deal with an unknown number of objects will need to use a linked list.

One disadvantage of a linked list against an array is that it does not allow direct access to the individual elements.
If you want to access a particular item then you have to start at the head and follow the references until you get to that item.
Another disadvantage is that a linked list uses more memory compared to an array. We need an extra 4 bytes (on 32-bit CPU) to store a reference 
to the next node. 

There are 2 types of linked lists: 
- Singly linked list (what I've described above)
- Doubly linked list (which has to references: one to the next node, and another to the previous node)
*/
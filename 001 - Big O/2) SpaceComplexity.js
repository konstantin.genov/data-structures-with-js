// Space complexity is measured by the amount of memory space a function requires to solve a problem.
// This means we do not take the memory space/size of the input which is passed to the function, but the memory allocated by the function's resolution. 

// Example 1
function boo(n){
    for (let i = 0; i < n.length; i++){
        console.log('Boo!');
    }
}

boo([1,2,3,4,5]); // This is O(1) as the function does not allocate any memory to new variables, data structures, etc. to accomplish it's purpose.

function helloNTimes(n){
    let helloArray = []; // O(1)
    for (let i = 0; i < n; i++){ 
        helloArray[i] = 'Hello'; // O(n)
    }
    return helloArray; // O(1 + n) -> O(n). This function requires additional memory space because it adds 'n' items('Hello') to the array and the data set grows.
}
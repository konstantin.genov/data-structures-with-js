// O(n!) - factorial time complexity. You should NEVER achieve code which is O(n!)
// It is highly unlikely you would come across such code. You can take a look at the Traveling Salesman Problem (TSP) with brute-force search as an example.

// Below, I will provide an algorithmic example by calculating all the permutations of a string

function getPermutations(string, prefix = '') {
    if(string.length <= 1) {
      return [prefix + string];
    }
  
    return Array.from(string).reduce((result, char, index) => {
      const reminder = string.slice(0, index) + string.slice(index+1);
      result = result.concat(getPermutations(reminder, prefix + char));
      return result;
    }, []);
  }


  getPermutations('ab') // ab, ba...
  // n = 2, f(n) = 2;
  getPermutations('abc') // abc, acb, bac, bca, cab, cba...
  // n = 3, f(n) = 6;
  getPermutations('abcd') // abcd, abdc, acbd, acdb, adbc, adcb, bacd...
  // n = 4, f(n) = 24;
  getPermutations('abcde') // abcde, abced, abdce, abdec, abecd, abedc, acbde...
  // n = 5, f(n) = 120;

// As you continue to add more letters to the input, f(n) will grow with an extremely alarming rate.
// Below you can see what would happen if we run it with 10 letters.
//## getPermutations('abcdefghij') // => abcdefghij, abcdefghji, abcdefgihj, abcdefgijh, abcdefgjhi, abcdefgjih, abcdefhgij...
// ## // n = 10, f(n) = 3,628,800; -> Woah!

 /*
 O(1) - Constant time.
 This means that a function will perform only 1 thing, thus it has constant time complexity.
 For example, if we have a function that takes an array and outputs the first element, we are always performing one operation - it doesn't matter how big the array is.
 A function which increases it's number of operations depending on the number of elements in an array is linear [O(n)].
 When the # of operations don't increase, the time complexity is constant - O(1)
 */

 const boxes = [0,1,2,3,4,5];
 function logFirstTwoBoxes(boxes){
     console.log(boxes[0]); // O(1)
     console.log(boxes[1]); // O(1)
 }

 logFirstTwoBoxes(boxes); // O(2)

 // Each time, the above function runs 2 operations -> O(2). No matter how big the boxes get, the number of operations is going to be 2.
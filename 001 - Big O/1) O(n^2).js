// Log all pairs of array

const boxes = ['a', 'b', 'c', 'd', 'e'];

function logAllPairs(array){
    for(let i = 0; i < array.length; i++){ // O(n)
        for (let j = 0; j < array.length; j++){ // O(n)
            console.log(array[i], array[j]);
        }
    }
}

logAllPairs(boxes); // O(n * n ) -> O(n^2)

// O(n^2) represents an algorithm whose performance is directly proportional to the square of the size 
// of the input data set. This is very common with nested loops over the data set. 
// In the above example, we iterate through the array n * n times, for each element in the array, we iterate the entire array again
// So, when we start at 'a', we iterate through all of the elements, then we repeat this for the remaining elements/size of the dataset -> O(n^2)
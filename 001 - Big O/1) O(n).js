const nemo = ['nemo'];
const large = new Array(1000000).fill('nemo');

// The below function is rated O(n) by the standards of the Big O notation because the time complexity depends on the number of elements introduced to the function. 
// Meaning, the more items that we have, the more our growth rate will increase linearly. -> O(n)
// Example:
// Passing in the nemo array to the function: O(n) = O(1) as the array has only 1 element
// Passing in the large array to the function = O(n) = O(1000000)  
function findNemo(array){
    for (let i = 0; i < array.length; i++){
        if(array[i] === 'nemo'){
            console.log('Found Nemo!');
        }
    }
      }

findNemo(nemo); // O(n) 